import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, Injectable, InjectionToken, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule as NgRxStoreModule, ActionReducerMap, Store } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { HttpClient, HttpClientModule, HttpHeaders, HttpRequest } from '@angular/common/http';

import { AppComponent } from './app.component';
import { ComicComponent } from './components/comic/comic.component';
import { ListaComicsComponent } from './components/lista-comics/lista-comics.component';
import { ComicDetalleComponent } from './components/comic-detalle/comic-detalle.component';
import { FormComicComponent } from './components/form-comic/form-comic.component';
import { 
  ComicsState,
  reducerComics,
  intializeComicsState,
  ComicsEffects,
  InitMyDataAction
} from './models/comics-state.model';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { AuthService } from './services/auth.service';
import { PersonajesComponentComponent } from './components/personajes/personajes-component/personajes-component.component';
import { PersonajesMainComponentComponent } from './components/personajes/personajes-main-component/personajes-main-component.component';
import { PersonajesMasInfoComponentComponent } from './components/personajes/personajes-mas-info-component/personajes-mas-info-component.component';
import { PersonajesDetalleComponent } from './components/personajes/personajes-detalle-component/personajes-detalle-component.component';
import { ReservasModule } from './reservas/reservas.module';

// init routing
export const childrenRoutesPersonajes: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: PersonajesMainComponentComponent },
  { path: 'mas-info', component: PersonajesMasInfoComponentComponent },
  { path: ':id', component: PersonajesDetalleComponent },
];


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: ListaComicsComponent },
  { path: 'comic/:id', component: ComicDetalleComponent },
  { path: 'login', component: LoginComponent },
  {
    path: 'protected',
    component: ProtectedComponent,
    canActivate: [ UsuarioLogueadoGuard ]
  },
  {
    path: 'personajes',
      component: PersonajesComponentComponent,
      canActivate: [ UsuarioLogueadoGuard ],
      children: childrenRoutesPersonajes
  }
];
// fin routing

//redux init
export interface AppState {
  comics: ComicsState;
}

const reducers: ActionReducerMap<AppState> = {
  comics: reducerComics
};

let reducersInitialState = {
  comics: intializeComicsState()
};
// fin redux

// app config
export interface AppConfig {
  apiEndpoint: String;
}
const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'http://localhost:3000'
};
export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
// fin app config

// app init
export function init_app(appLoadService: AppLoadService): () => Promise<any>  {
  return () => appLoadService.intializeComicsState();
}

@Injectable()
class AppLoadService {
  constructor(private store: Store<AppState>, private http: HttpClient) { }
  async intializeComicsState(): Promise<any> {
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint + '/my', { headers: headers });
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));
  }
}

// fin app init


@NgModule({
  declarations: [
    AppComponent,
    ComicComponent,
    ListaComicsComponent,
    ComicDetalleComponent,
    FormComicComponent,
    LoginComponent,
    ProtectedComponent,
    PersonajesComponentComponent,
    PersonajesMainComponentComponent,
    PersonajesMasInfoComponentComponent,
    PersonajesDetalleComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    NgRxStoreModule.forRoot(reducers, {
      initialState: reducersInitialState,
      runtimeChecks: {
        strictStateImmutability: false,
        strictActionImmutability: false,
      }
    }),
    EffectsModule.forRoot([ComicsEffects]),
    StoreDevtoolsModule.instrument(),
    ReservasModule
  ],
  providers: [ 
    AuthService,
    UsuarioLogueadoGuard,
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    AppLoadService,
    { provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
