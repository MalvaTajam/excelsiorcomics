import { v4 as uuid } from 'uuid';

export class Comic {
	selected: boolean;
	caracteristicas: string[];
	id = uuid();

	constructor(public nombre: string, public imagenUrl: string, public votes: number = 0) { 
		this.caracteristicas = ['Tapa dura', 'Hoja fotografía'];
	}
	isSelected() {
		return this.selected;
	}
	setSelected(s: boolean) {
		this.selected = s;
	}
	voteUp() {
		this.votes++;
	}
	voteDown() {
		this.votes--;
	}
}
