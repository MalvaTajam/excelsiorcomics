import { forwardRef, Inject, Injectable } from '@angular/core';
import { Comic } from './comic.model';
import { Store } from '@ngrx/store';
import {
	NuevoComicAction,
	ElegidoFavoritoAction
} from './comics-state.model';
import { AppConfig, AppState, APP_CONFIG } from './../app.module';
import { HttpClient, HttpClientModule, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';

@Injectable()
export class ComicsApiClient {
	comics: Comic[] = [];

	constructor(
    	private store: Store<AppState>,
    	@Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
    	private http: HttpClient
  	) {
    	this.store
      		.select(state => state.comics)
      		.subscribe((data) => {
        		console.log('comics sub store');
        		console.log(data);
        		this.comics = data.items;
      		});
    	this.store
      		.subscribe((data) => {
        		console.log('all store');
        		console.log(data);
      		});
    }
	add(c: Comic){
		const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
		const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: c.nombre }, { headers: headers });
		this.http.request(req).subscribe((data: HttpResponse<{}>) => {
		  if (data.status === 200) {
			this.store.dispatch(new NuevoComicAction(c));
		  }
		});
	}

	getById(id: String): Comic {
		return this.comics.filter(function(c) { return c.id.toString() === id; })[0];
	}

	getAll(): Comic[] {
		return this.comics;
	}

	elegir(c: Comic) {
		// aqui incovariamos al servidor
		this.store.dispatch(new ElegidoFavoritoAction(c));
	}
} 