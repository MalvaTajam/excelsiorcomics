import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Comic } from './comic.model';
import { HttpClientModule } from '@angular/common/http';

// ESTADO
export interface ComicsState {
    items: Comic[];
    loading: boolean;
    favorito: Comic;
}

export function intializeComicsState() {
	return {
	  items: [],
	  loading: false,
	  favorito: null
	};
}

// ACCIONES
export enum ComicsActionTypes {
  NUEVO_COMIC = '[Comics] Nuevo',
  ELEGIDO_FAVORITO = '[Comics] Favorito',
  VOTE_UP = '[Comic] Vote Up',
  VOTE_DOWN = '[Comic] Vote Down',
  INIT_MY_DATA = '[Comics] Init My Data'
}

export class NuevoComicAction implements Action {
  type = ComicsActionTypes.NUEVO_COMIC;
  constructor(public comic: Comic) {}
}

export class ElegidoFavoritoAction implements Action {
  type = ComicsActionTypes.ELEGIDO_FAVORITO;
  constructor(public comic: Comic) {}
}

export class VoteUpAction implements Action {
	type = ComicsActionTypes.VOTE_UP;
	constructor(public comic: Comic) {}
}

export class VoteDownAction implements Action {
	type = ComicsActionTypes.VOTE_DOWN;
	constructor(public comic: Comic) {}
}

export class InitMyDataAction implements Action {
	type = ComicsActionTypes.INIT_MY_DATA;
	constructor(public comics: string[]) {}
}

export type ComicsActions = NuevoComicAction | ElegidoFavoritoAction
	| VoteDownAction | VoteDownAction | InitMyDataAction;

// REDUCERS
export function reducerComics(
	state: ComicsState,
	action: ComicsActions
): ComicsState {
	switch (action.type) {
		case ComicsActionTypes.INIT_MY_DATA: {
			const comics: string[] = (action as InitMyDataAction).comics;
			return {
				...state,
				items: comics.map((c) => new Comic(c, ''))
			  };
		}
		case ComicsActionTypes.NUEVO_COMIC: {
		  return {
		  		...state,
		  		items: [...state.items, (action as NuevoComicAction).comic ]
		  	};
		}
		case ComicsActionTypes.ELEGIDO_FAVORITO: {
		    state.items.forEach(x => x.setSelected(false));
		    let fav: Comic = (action as ElegidoFavoritoAction).comic;
		    fav.setSelected(true);
		    return {
		    	...state,
		  		favorito: fav
			};
		}
		case ComicsActionTypes.VOTE_UP: {
		    const c: Comic = (action as VoteUpAction).comic;
		    c.voteUp();
		    return { ...state };
		}
		case ComicsActionTypes.VOTE_DOWN: {
		    const c: Comic = (action as VoteDownAction).comic;
		    c.voteDown();
		    return { ...state };
		}
	}
	return state;
}

// EFFECTS
@Injectable()
export class ComicsEffects {
  @Effect()
  nuevoAgregado$: Observable<Action> = this.actions$.pipe(
    ofType(ComicsActionTypes.NUEVO_COMIC),
  	map((action: ComicsActions) => new ElegidoFavoritoAction(action.comic))
  );

  constructor(private actions$: Actions) {}
} 