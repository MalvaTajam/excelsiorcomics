import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonajesMainComponentComponent } from './personajes-main-component.component';

describe('PersonajesMainComponentComponent', () => {
  let component: PersonajesMainComponentComponent;
  let fixture: ComponentFixture<PersonajesMainComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonajesMainComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonajesMainComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
