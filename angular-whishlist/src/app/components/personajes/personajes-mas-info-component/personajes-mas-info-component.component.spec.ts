import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonajesMasInfoComponentComponent } from './personajes-mas-info-component.component';

describe('PersonajesMasInfoComponentComponent', () => {
  let component: PersonajesMasInfoComponentComponent;
  let fixture: ComponentFixture<PersonajesMasInfoComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonajesMasInfoComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonajesMasInfoComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
