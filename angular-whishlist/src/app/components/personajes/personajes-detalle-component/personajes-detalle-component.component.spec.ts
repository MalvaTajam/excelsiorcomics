import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonajesDetalleComponentComponent } from './personajes-detalle-component.component';

describe('PersonajesDetalleComponentComponent', () => {
  let component: PersonajesDetalleComponentComponent;
  let fixture: ComponentFixture<PersonajesDetalleComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonajesDetalleComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonajesDetalleComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
