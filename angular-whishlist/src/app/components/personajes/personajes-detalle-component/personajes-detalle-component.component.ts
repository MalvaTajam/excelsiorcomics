import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-personajes-detalle-component',
  templateUrl: './personajes-detalle-component.component.html',
  styleUrls: ['./personajes-detalle-component.component.css']
})
export class PersonajesDetalleComponent implements OnInit {
  id: any;

  constructor(private route: ActivatedRoute) {
    route.params.subscribe(params => { this.id = params['id']; });
  }
  ngOnInit() {
  }

}