import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonajesComponentComponent } from './personajes-component.component';

describe('PersonajesComponentComponent', () => {
  let component: PersonajesComponentComponent;
  let fixture: ComponentFixture<PersonajesComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonajesComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonajesComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
