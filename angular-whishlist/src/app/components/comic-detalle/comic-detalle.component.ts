import { Component, OnInit, } from '@angular/core';
import { ComicsApiClient} from './../../models/comics-api-client.model';
import { Comic } from './../../models/comic.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-comic-detalle',
  templateUrl: './comic-detalle.component.html',
  styleUrls: ['./comic-detalle.component.css'],
  providers: [ComicsApiClient]
})
export class ComicDetalleComponent implements OnInit {
  [x: string]: any;
  comic: Comic;

  constructor(private route: ActivatedRoute, private destinosApiClient: ComicsApiClient) {}

  ngOnInit() {
    let id = this.route.snapshot.paramMap.get('id');
    this.comic = this.comicsApiClient.getById(id);
  }

}
