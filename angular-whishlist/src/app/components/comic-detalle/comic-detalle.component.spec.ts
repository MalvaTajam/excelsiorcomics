import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComicDetalleComponent } from './comic-detalle.component';

describe('ComicDetalleComponent', () => {
  let component: ComicDetalleComponent;
  let fixture: ComponentFixture<ComicDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComicDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComicDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
