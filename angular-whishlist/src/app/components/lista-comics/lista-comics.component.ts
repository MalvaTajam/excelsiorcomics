import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import { Comic } from './../../models/comic.model';
import { ComicsApiClient } from './../../models/comics-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from './../../app.module';
import { ComicsState } from './../../models/comics-state.model';

@Component({
  selector: 'app-lista-comics',
  templateUrl: './lista-comics.component.html',
  styleUrls: ['./lista-comics.component.css'],
  providers: [ComicsApiClient]
})
export class ListaComicsComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<Comic>;
  updates: string[];
  all;

  constructor(public comicsApiClient: ComicsApiClient, public store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state => state.comics.favorito)
      .subscribe(c => {
        if (c != null) {
          this.updates.push('Se ha elegido a ' + c.nombre);
        }
      });
    this.all = store.select(state => state.comics.items).subscribe(items => this.all = items); 
  }

  ngOnInit() {
  }

  agregado(c: Comic) {
    this.comicsApiClient.add(c);
    this.onItemAdded.emit(c);
  }

  elegido(e: Comic) {
    this.comicsApiClient.elegir(e);
  }
  getAll() {

  }
}
