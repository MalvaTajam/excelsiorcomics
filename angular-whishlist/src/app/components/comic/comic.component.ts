import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { Comic } from './../../models/comic.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { VoteUpAction, VoteDownAction } from './../../models/comics-state.model';

@Component({
  selector: 'app-comic',
  templateUrl: './comic.component.html',
  styleUrls: ['./comic.component.css']
})
export class ComicComponent implements OnInit {
  @Input() comic: Comic;
  @Input('idx') position: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() onClicked: EventEmitter<Comic>;

  constructor(public store: Store<AppState>) {
    this.onClicked = new EventEmitter();
  }

  ngOnInit() {
  }

  ir() {
    this.onClicked.emit(this.comic);
    return false;
  }

  voteUp() {
    this.store.dispatch(new VoteUpAction(this.comic));
    return false;
  }

  voteDown() {
    this.store.dispatch(new VoteDownAction(this.comic));
    return false;
  }

}
