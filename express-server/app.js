var express = require("express"), cors = require('cors');
var app = express();
app.use(express.json());
app.use(cors());
app.listen(3000, () => console.log("Server running on port 3000"));

var titulos = [ "Batman - Final de Juego", "Dinastia de M", "X-Men - Complejo de Mesías", "Secret Wars", "Dr. Strange - El origen de Dr. Strange", "The Amazing Spider-Man - La identidad del Duende", "Wolverine - Origen" ];
app.get("/titulos", (req, res, next) => res.json(titulos.filter((c)=> c.toLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1)));

var misComics = [];
app.get("/my", (req, res, next) => res.json(misComics));
app.post("/my", (req, res, next) => {
  console.log(req.body);
  misComics.push(req.body.nuevo);
  res.json(misComics);
}); 